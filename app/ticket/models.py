from django.contrib.auth.models import User
from django.db import models


class Status(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.name


class Ticket(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=200, blank=True, default='')
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True,
                              verbose_name='owner', blank=True,
                              related_name='tickets')
    support = models.ForeignKey('auth.User', on_delete=models.CASCADE,
                                null=True, blank=True, verbose_name='support')
    status = models.ForeignKey(Status, on_delete=models.CASCADE,
                               null=True, blank=True, related_name='status')
    date_creation = models.DateTimeField(auto_now_add=True, null=True,
                                         blank=True)

    def __str__(self) -> str:
        return self.title

    def get_owner(self) -> User:
        return self.owner

    class Meta:
        ordering = ['date_creation']


class TicketFlow(models.Model):
    ticket = models.ForeignKey('Ticket', on_delete=models.CASCADE,
                               related_name='messages')
    message = models.CharField(max_length=200)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE,
                             related_name='messages')
    date_message = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.message

    class Meta:
        ordering = ['date_message']
