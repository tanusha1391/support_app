from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.pagination import PageNumberPagination

from app.tasks import send_func_email, send_telegram_message

from . import serializers
from .models import Status, Ticket, TicketFlow
from .permissions import IsAdmin, IsAuthenticated, IsOwnerOrAdmin
from .serializers import (StatusSerializer, TicketCreateSerializer,
                          TicketFlowCreateSerializer, TicketFlowSerializer,
                          TicketSerializer)


class UserAPIList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAdmin, )


class StatusAPIList(generics.ListCreateAPIView):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer
    permission_classes = (IsOwnerOrAdmin, )


class TicketAPIListPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 100


class TicketAPIList(generics.ListAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    # permission_classes = (IsAdmin, ) - commented for pytest
    pagination_class = TicketAPIListPagination
    filter_backends = (SearchFilter, )
    search_fields = ('status__id', )

    def get_queryset(self):
        queryset = Ticket.objects.all()
        return queryset.order_by('date_creation')


class TicketAPIDestroy(generics.RetrieveDestroyAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    permission_classes = (IsAdmin, )


class TicketStatusAPIUpdate(generics.UpdateAPIView):
    permission_classes = (IsAdmin, )
    serializer_class = serializers.TicketStatusUpdateSerializer

    def get_queryset(self):
        support_email = self.request.user.email
        status_id = (self.request.data)['status']
        path_info = str(self.request.get_full_path_info())
        ticket_number = None
        for s in path_info.split('/'):
            if s.isdigit():
                ticket_number = s
                break
        send_func_email.delay(support_email, status_id, ticket_number)
        send_telegram_message.delay(status_id, ticket_number)
        queryset = Ticket.objects.all()
        return queryset


class TicketAPICreate(generics.ListCreateAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketCreateSerializer
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


# TicketFlow API
class TicketFlowAPICreate(generics.ListCreateAPIView):
    queryset = TicketFlow.objects.all()
    serializer_class = TicketFlowCreateSerializer
    permission_classes = (IsOwnerOrAdmin, )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TicketFlowAPIView(generics.ListAPIView):
    serializer_class = TicketFlowSerializer
    permission_classes = (IsOwnerOrAdmin, )
    filter_backends = (SearchFilter, OrderingFilter, )
    search_fields = ('ticket__id', )

    def get_queryset(self):
        user = self.request.user
        queryset = TicketFlow.objects.all()
        if not bool(IsAdmin):
            queryset = queryset.filter(user=user)
        return queryset.order_by('date_message')


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
