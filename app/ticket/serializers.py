from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Status, Ticket, TicketFlow


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = "__all__"


class TicketSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Ticket
        fields = "__all__"


class TicketStatusUpdateSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.email')
    support = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Ticket
        fields = ['id', 'status', 'owner', 'support', ]


class TicketCreateSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    messages = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Ticket
        fields = ['id', 'title', 'description', 'owner', 'messages']


class TicketFlowCreateSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = TicketFlow
        fields = ['id', 'ticket', 'message', 'user', 'date_message']


class TicketFlowSerializer(serializers.ModelSerializer):
    # user = serializers.ReadOnlyField(source='owner')

    class Meta:
        model = TicketFlow
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    tickets = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    messages = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'tickets', 'messages']
