from datetime import datetime

import requests
from django.core.mail import send_mail

from ticket.models import Status, Ticket

from .settings import EMAIL_HOST_USER, TELEGRAM_BOT_URL, TELEGRAM_CHAT_ID


def send(support_email, stat_id, ticket_number):
    if stat_id is not None:
        stat = Status.objects.get(id=stat_id)
        owner = Ticket.objects.get(id=ticket_number).get_owner()
        send_mail(
            'Ticket #' + str(ticket_number),
            'New Status: ' + str(stat).upper(),
            EMAIL_HOST_USER,
            [support_email],
            fail_silently=False,
        )

        send_mail(
            'Ticket #' + str(ticket_number),
            'New Status: ' + str(stat).upper(),
            EMAIL_HOST_USER,
            [owner],
            fail_silently=False,
        )


def send_telegram(stat_id, ticket_number):
    if stat_id is not None:
        stat = Status.objects.get(id=stat_id)
        cu_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        msg = '♻️♻️⚜️⚜️⚜️♻️♻️\n' + cu_date
        msg += '\n📨 Ticket  #<code>' + str(ticket_number)
        msg += '</code>\n🆕Status: <b>' + str(stat).upper() + "</b>"
        params = {'chat_id': TELEGRAM_CHAT_ID, 'text': msg,
                  'disable_web_page_preview': True, 'parse_mode': 'HTML'}
        method = 'sendMessage'
        requests.post(TELEGRAM_BOT_URL + method, params)
