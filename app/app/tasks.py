from .celery import app
from .service import send, send_telegram

"""
this function sends email using celery and redis
"""
@app.task
def send_func_email(support_email, status_id, ticket_number):
    send(support_email, status_id, ticket_number)


"""
this function sends messages to telegram app using celery and redis
"""
@app.task
def send_telegram_message(status_id, ticket_number):
    send_telegram(status_id, ticket_number)
