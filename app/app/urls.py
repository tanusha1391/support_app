from django.contrib import admin
from django.urls import include, path
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView, TokenVerifyView)

from ticket.views import (TicketAPICreate, TicketAPIDestroy, TicketAPIList,
                          TicketFlowAPICreate, TicketFlowAPIView,
                          TicketStatusAPIUpdate)

# router = CustomRouter()
# router.register(r'ticket', TicketView, basename='ticket')
# # router.register(r'ticket/add', TicketViewCreate, basename='ticketadd')
# router.register(r'ticket/del', TicketViewDestroy, basename='ticketdel')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include('rest_framework.urls')),
    # List of all tickets or filter by status
    path('api/tickets', TicketAPIList.as_view()),
    # Update Selected Ticket
    path('api/ticket/<int:pk>/update/', TicketStatusAPIUpdate.as_view()),
    # Create a new ticket
    path('api/ticket/new/', TicketAPICreate.as_view()),
    # Delete a full data by ticket (access only for admin)
    path('api/ticket/<int:pk>/delete/', TicketAPIDestroy.as_view()),

    # View all messages by ticket number
    path('api/ticket/', TicketFlowAPIView.as_view()),
    # Add a message in the thread of ticket
    path('api/addmessage/', TicketFlowAPICreate.as_view()),

    # using JWT getting refresh and access tokens
    path('api/token/', TokenObtainPairView.as_view()),
    # using JWT apply refresh to access token by refresh token
    path('api/token/refresh/', TokenRefreshView.as_view()),
    # using JWT check and verify access token
    path('api/token/verify/', TokenVerifyView.as_view()),
]
