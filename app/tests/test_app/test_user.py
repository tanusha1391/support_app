import uuid

import pytest
from django.contrib.auth.models import User


@pytest.fixture
def create_user(db, django_user_model, test_password):
    def make_user(**kwargs):
        kwargs['password'] = test_password
        if 'username' not in kwargs:
            kwargs['username'] = str(uuid.uuid4())
        return django_user_model.objects.create_user(**kwargs)
    return make_user


@pytest.fixture
def test_password():
    return 'strong-test-pass'


@pytest.mark.django_db
def test_auth_view(client, create_user, test_password):
    user = create_user()
    url = '/api/auth/login/'
    client.login(
        username=user.username, password=test_password
    )
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_user_create():
    User.objects.create_user('John', 'johnsmith@gmail.com', 'Longpassw0rdhere')
    assert User.objects.count() == 1


@pytest.mark.django_db
def test_login_user(client):
    payload = dict(
        username="lagerta",
        password="13",
    )
    response = client.post("/api/auth/login/", payload)
    assert response.status_code == 200
