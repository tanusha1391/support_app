import pytest


@pytest.mark.django_db
def test_list_tickets(client):
    # list tickets awailable for all users (see view permissions)
    response = client.get("/api/tickets")
    assert response.status_code == 200


@pytest.mark.django_db
def test_create_ticket(client):
    # unauthorized user can not create tickets
    # status error = 403
    payload = dict(
        title="ticket #xx",
        description="test for creation ticket",
    )
    response = client.post("/api/ticket/new/", payload)
    assert response.status_code == 200
